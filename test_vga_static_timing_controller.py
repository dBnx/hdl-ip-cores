"""
Example of a simple testbench for a RAM block
"""
import random

import cocotb
from cocotb.clock import Clock
from cocotb.triggers import Timer, RisingEdge, ReadOnly
from cocotb.result import TestError, TestFailure, ReturnValue


# @cocotb.coroutine
# def write_ram_sp(dut, address, value):
#     """This coroutine performs a write of the RAM"""
#     yield RisingEdge(dut.clk_write)              # Synchronise to the read clock
#     dut.address_write = address                  # Drive the values
#     dut.data_write = value
#     dut.write_enable = 1
#     yield RisingEdge(dut.clk_write)              # Wait 1 clock cycle
#     dut.write_enable = 0                        # Disable write

# dut.clk_pixel,
# dut.enable,
# dut.reset,
#
# dut.position_x,
# dut.position_y,
# dut.position_valid,
# dut.vga_vsync,
# dut.vga_hsync


def dut_to_str(dut) -> str:
    return "DUT{{ " \
        f"Position(valid={dut.position_valid}): "\
        f"({dut.position_x.value}, {dut.position_y.value})"\
        " }}"


@cocotb.coroutine
def reset(dut):
    dut.reset.value = 0
    dut.enable.value = 0
    yield RisingEdge(dut.clk_pixel)  # Synchronise to the read clock
    dut.enable.value = 1
    dut.reset.value = 1
    yield RisingEdge(dut.clk_pixel)  # Synchronise to the read clock
    dut.reset.value = 0

    if dut.position_x.value != 0 or dut.position_y.value != 0:
        dut._log.error("After reset position is nonzero:"
                       f" ({dut.position_x.value}, {dut.position_y.value})")
        raise TestFailure("Invalid position")


@cocotb.coroutine
def horizontal_line(dut, expected_y: int, verbose=False):
    # TODO: Assertions on HSync and position valid
    if verbose:
        dut._log.info("H active ...")
    for i in range(int(dut.H_ACTIVE)):
        yield RisingEdge(dut.clk_pixel)
        if dut.position_x != i or dut.position_y != expected_y:
            dut._log.error(f"Invalid position after {i} h pixel clocks: "
                           f" ({dut.position_x.value}, {dut.position_y.value})"
                           f" should: ({i}, 0{expected_y:b})")
            raise TestFailure("Invalid position x")
        if verbose:
            dut._log.info(
                f"{i:>3} ({dut.position_x.value}, {dut.position_y.value})")

    if verbose:
        dut._log.info("H blank ...")
    yield RisingEdge(dut.clk_pixel)
    for i in range(int(dut.H_TOTAL) - int(dut.H_ACTIVE)):
        yield RisingEdge(dut.clk_pixel)
        if dut.position_valid != 0:
            clks = int(dut.H_ACTIVE) + i
            dut._log.error(f"Position valid in H blank after {clks} pixel clocks: "
                           f"Active: {dut.position_valid.value}"
                           f" ({dut.position_x.value}, {dut.position_y.value})")
            raise TestFailure("Invalid position_valid")


@cocotb.coroutine
def complete_frame(dut):
    # TODO: Assertions on HSync and position valid
    v_active_lines = int(dut.V_ACTIVE)
    dut._log.info(f"V active for {v_active_lines} ...")
    for i in range(v_active_lines):
        yield horizontal_line(dut, i)
        # if dut.position_x != 0 or dut.position_y != i:
        if dut.position_y != i:
            dut._log.error(f"Invalid position after {i} h-lines: "
                           f" ({dut.position_x.value}, {dut.position_y.value})")
            raise TestFailure("Invalid position y")
        val_x = dut.position_x.value
        val_y = dut.position_y.value
        dut._log.info(f"{i:>2} ({val_x}, {val_y})")

    v_blank_lines = int(dut.V_TOTAL) - int(dut.V_ACTIVE)
    dut._log.info(f"V blank for {v_blank_lines} h lines ...")
    h_position_bits: int = (int(dut.H_ACTIVE) - 1).bit_length() + 1
    h_position_mask: int = 1 << h_position_bits - 1
    for i in range(v_blank_lines):
        y_expected = int(dut.V_ACTIVE) + i
        yield horizontal_line(dut, y_expected % h_position_mask)
        val_x = dut.position_x.value
        val_y = dut.position_y.value
        dut._log.info(f"{y_expected:>2} ({val_x}, {val_y})")

        if dut.position_valid != 0:
            dut._log.error(f"Position valid in V blank after {y_expected} h lines: "
                           f"Active: {dut.position_valid.value}"
                           f" ({dut.position_x.value}, {dut.position_y.value})")
            raise TestFailure("Invalid position_valid")
        # if dut.position_valid != 0:
        #    lines = int(dut.V_ACTIVE) + i
        #    dut._log.error(f"Position valid in V blank after {lines} H lines: "
        #                   f"Active: {dut.position_valid.value}"
        #                   f" ({dut.position_x.value}, {dut.position_y.value})")
        #    raise TestFailure("Invalid position_valid")
    # TODO: Post checks


@cocotb.test()
def single_hline(dut):
    """Try writing values into the RAM and reading back"""
    # RAM = {}
    cocotb.start_soon(Clock(dut.clk_pixel, int(dut.H_TOTAL) + 10).start())

    dut._log.info("Reset ...")
    yield reset(dut)
    dut._log.info("H line ...")
    yield horizontal_line(dut, 0, verbose=True)


@cocotb.test()
def simple_reset(dut):
    """Check state after reset."""
    cocotb.start_soon(Clock(dut.clk_pixel, 10).start())

    dut._log.info("Reset ...")
    yield reset(dut)
    if dut.position_valid != 1 or dut.position_x != 0 or dut.position_y != 0:
        dut._log.error(f"Invalid values after reset: position_valid: "
                       f"{dut_to_str(dut)}")
        raise TestFailure("Invalid position_valid")
    dut._log.info("Reset OK")


@cocotb.test()
def not_enabled(dut):
    """Reset and the pass a whole frame, while not being enabled."""

    cocotb.start_soon(Clock(dut.clk_pixel, 20).start())

    dut._log.info("Reset ...")
    yield reset(dut)
    dut.enable = 0

    old_x = dut.position_x.value
    old_y = dut.position_y.value
    dut._log.info(f"(Should be) doing nothing ...old: ({old_x}, {old_y})")
    for i in range(int(dut.V_TOTAL) * int(dut.H_TOTAL)):
        yield RisingEdge(dut.clk_pixel)
        if dut.position_valid != 0 \
                or dut.position_x != old_x or dut.position_y != old_y \
                or dut.vga_vsync != 1 or dut.vga_hsync != 1:
            dut._log.error(f"State change even though disabled @ {i} clk: "
                           f"{dut_to_str(dut)}")
            raise TestFailure("Invalid position_valid")
    dut._log.info("Reset OK")


@cocotb.test()
def multiple_visible_h_lines(dut):
    LINES = 3
    CLK_PER_LINE = dut.H_TOTAL.value * LINES + 10
    cocotb.start_soon(Clock(dut.clk_pixel, CLK_PER_LINE).start())
    if int(dut.V_ACTIVE.value) < LINES:
        dut._log.error("Invalid configuration: Should simulate more visible"
                       " lines than there are in a frame")
        raise TestError("Invalid configuration: `LINES` > `dut.V_ACTIVE`")

    dut._log.info("Reset ...")
    yield reset(dut)

    dut._log.info("Lines ...")
    for i in range(LINES):
        yield horizontal_line(dut, i, verbose=True)


@cocotb.test()
def full_frame(dut):
    """Reset and the pass a whole frame, while not being enabled."""
    N_FRAMES = 1
    CLK_PER_FRAME = dut.V_TOTAL.value * dut.H_TOTAL.value * N_FRAMES + 10
    cocotb.start_soon(Clock(dut.clk_pixel, CLK_PER_FRAME).start())

    dut._log.info("Reset ...")
    yield reset(dut)

    dut._log.info("Frames ...")
    for i in range(N_FRAMES):
        dut._log.info(f"<<< Frame {i} >>>")
        yield complete_frame(dut)

    # TODO: impl

    # # Read the parameters back from the DUT to set up our model
    # width = dut.D_WIDTH.value.integer
    # depth = 2**dut.A_WIDTH.value.integer
    # dut.log.info("Found %d entry RAM by %d bits wide" % (depth, width))

    # # Set up independent read/write clocks
    # cocotb.fork(Clock(dut.clk_write, 3200).start())
    # cocotb.fork(Clock(dut.clk_read, 5000).start())

    # dut.log.info("Writing in random values")
    # for i in range(depth):
    #     RAM[i] = int(random.getrandbits(width))
    #     yield write_ram_sp(dut, i, RAM[i])

    # dut.log.info("Reading back values and checking")
    # for i in range(depth):
    #     value = yield read_ram_sp(dut, i)
    #     if value != RAM[i]:
    #         dut.log.error("RAM[%d] expected %d but got %d" %
    #                       (i, RAM[i], dut.data_read.value.value))
    #         raise TestFailure("RAM contents incorrect")
    # dut.log.info("RAM contents OK")

module vga_static_timing_controller #(
    parameter int CLK_PIXEL_HZ = 10_000,
    parameter int V_ACTIVE = 100,
    parameter int V_PORCH_FRONT = 10,
    parameter int V_SYNC = 10,
    parameter int V_PORCH_BACK = 10,
    parameter int H_ACTIVE = 100,
    parameter int H_PORCH_FRONT = 10,
    parameter int H_SYNC = 10,
    parameter int H_PORCH_BACK = 10
) (
    // --- INPUT ---
    input logic clk_pixel,
    input logic enable,
    input logic reset,

    // --- OUTPUT ---
    output logic [$clog2(H_ACTIVE)-1:0] position_x,
    output logic [$clog2(V_ACTIVE)-1:0] position_y,
    output logic                        position_valid,
    output logic                        vga_vsync,
    output logic                        vga_hsync
);
  // TODO: Assert params > 1, if -1 appears
  localparam V_TOTAL = V_ACTIVE + V_PORCH_FRONT + V_SYNC + V_PORCH_BACK;
  localparam H_TOTAL = H_ACTIVE + H_PORCH_FRONT + H_SYNC + H_PORCH_BACK;
  localparam V_WIDTH = $clog2(V_TOTAL);
  localparam H_WIDTH = $clog2(H_TOTAL);

  logic [V_WIDTH-1:0] v_ctr;
  logic [H_WIDTH-1:0] h_ctr;

  // TODO: Incorporate enable
  always_ff @(posedge clk_pixel or posedge reset) begin
    if (reset) begin
      h_ctr <= 0;
      v_ctr <= 0;
    end else if (enable) begin
      if (h_ctr < H_TOTAL[H_WIDTH-1:0]) begin
        h_ctr <= h_ctr + 1'b1;
      end else begin
        h_ctr <= 0;
        if (v_ctr < V_TOTAL[V_WIDTH-1:0]) begin
          v_ctr <= v_ctr + 1'b1;
        end else begin
          v_ctr <= 0;
        end
      end
    end
  end

  assign vga_hsync = ~(h_ctr > (H_ACTIVE[H_WIDTH-1:0] + H_PORCH_FRONT[H_WIDTH-1:0]) && h_ctr < (H_TOTAL[H_WIDTH-1:0] - H_PORCH_BACK[H_WIDTH-1:0]));
  assign vga_vsync = ~(v_ctr > (V_ACTIVE[V_WIDTH-1:0] + V_PORCH_FRONT[V_WIDTH-1:0]) && v_ctr < (V_TOTAL[V_WIDTH-1:0] - V_PORCH_BACK[V_WIDTH-1:0]));

  logic h_is_active, v_is_active;
  assign h_is_active = h_ctr < H_ACTIVE[H_WIDTH-1:0];
  assign v_is_active = v_ctr < V_ACTIVE[V_WIDTH-1:0];

  assign position_valid = h_is_active && v_is_active && enable;
  assign position_x = h_ctr[$clog2(H_ACTIVE)-1:0];
  assign position_y = v_ctr[$clog2(V_ACTIVE)-1:0];

endmodule
